package com.help.animal.animalhelp.Actividades;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.help.animal.animalhelp.Entidades.DatosAnimal;
import com.help.animal.animalhelp.Entidades.Usuario;
import com.help.animal.animalhelp.R;
import com.help.animal.animalhelp.navegacion;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.text.SimpleDateFormat;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityIngresoAnimal extends AppCompatActivity implements View.OnClickListener
{

    private EditText titulo, descripcion, ubicacion;
    private Button btnEnviar, btnFoto;
    private CircleImageView foto;
    private RadioButton rbPerro,rbGato ,rbOtro;
    private Uri uri;
    private String tipoAnimal, uuid, compraFecha, userId, nombreCompleto;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference, usuario;
    private StorageReference storage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso_animal);

        titulo = (EditText) findViewById(R.id.txtTitulo);
        descripcion = (EditText) findViewById(R.id.txtDescripcion);
        ubicacion = (EditText) findViewById(R.id.txtUbicacion);

        rbPerro= (RadioButton) findViewById(R.id.rbperro);
        rbGato= (RadioButton) findViewById(R.id.rbgato);
        rbOtro= (RadioButton) findViewById(R.id.rbotro);

        foto = (CircleImageView) findViewById(R.id.imgCargada);
        uri = null;
        uuid = UUID.randomUUID().toString();

        btnEnviar = (Button) findViewById(R.id.btnEnviarU);
        btnFoto = (Button) findViewById(R.id.btnFoto);

        btnEnviar.setOnClickListener(this);
        btnFoto.setOnClickListener(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        usuario = firebaseDatabase.getReference();
        databaseReference = firebaseDatabase.getReference("MASCOTAS");
        storage = FirebaseStorage.getInstance().getReference();

//        FirebaseUser user = firebaseAuth.getCurrentUser();
//        if (user.getUid() == null){
//            userId = "Anonimo";
//        }else{
//            userId = user.getUid();
//        }

        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            // do your stuff
            userId = user.getUid();
        } else {
            //signInAnonymously();
            userId = "Anonimo";
        }


        obtenerUsuario();
        fecha_hora();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEnviarU:
                guardarAnimal();
                break;
            case R.id.btnFoto:
                CropImage.activity()
                        .setAspectRatio(1, 1)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(this);
                break;
        }
    }

    private void guardarAnimal(){

        if(userId == "Anonimo"){
            nombreCompleto = "Anonimo";
            if (uri == null) {
                Toast.makeText(this, "DEBE ELEGIR UNA FOTO", Toast.LENGTH_SHORT).show();
                return;
            }else if (rbGato.isChecked()==false &&rbPerro.isChecked()==false&&rbOtro.isChecked()==false){
                Toast.makeText(this, "DEBE ELEGIR UN ANIMAL", Toast.LENGTH_SHORT).show();
                return;
            }else if (titulo.getText().toString().isEmpty()) {
                titulo.setError("INGRESE TITULO");
                titulo.requestFocus();
            }else if (ubicacion.getText().toString().isEmpty()) {
                ubicacion.setError("INGRESE UBICACION");
                ubicacion.requestFocus();
            }else if (descripcion.getText().toString().isEmpty()) {
                descripcion.setError("INGRESE DESCRIPCION");
                descripcion.requestFocus();
            }else{
                if(rbGato.isChecked() == true){
                    tipoAnimal = rbGato.getText().toString();
                }else if(rbPerro.isChecked() == true){
                    tipoAnimal = rbPerro.getText().toString();
                }else if(rbOtro.isChecked() == true){
                    tipoAnimal = rbOtro.getText().toString();
                }

                StorageReference file = storage.child("Mascotas").child(uri.getLastPathSegment());
                file.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        final Uri urlFotoAnimal = taskSnapshot.getDownloadUrl();

                        DatosAnimal datosAnimal = new DatosAnimal();
                        datosAnimal.setTitulo(titulo.getText().toString());
                        datosAnimal.setDescripcion(descripcion.getText().toString());
                        datosAnimal.setUbicacion(ubicacion.getText().toString());
                        datosAnimal.setTipoAnimal(tipoAnimal);
                        datosAnimal.setUrlFoto(urlFotoAnimal.toString());
                        datosAnimal.setFecha(compraFecha);
                        datosAnimal.setNombreUsuario(nombreCompleto);

                        databaseReference.child(uuid).setValue(datosAnimal).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ActivityIngresoAnimal.this, "ANIMAL REGISTRADO", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ActivityIngresoAnimal.this, login.class));
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ActivityIngresoAnimal.this, "NO SE A PODIDO REGISTRADO", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ActivityIngresoAnimal.this, login.class));
                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }

        }else{
            if (uri == null) {
                Toast.makeText(this, "DEBE ELEGIR UNA FOTO", Toast.LENGTH_SHORT).show();
                return;
            }else if (rbGato.isChecked()==false &&rbPerro.isChecked()==false&&rbOtro.isChecked()==false){
                Toast.makeText(this, "DEBE ELEGIR UN ANIMAL", Toast.LENGTH_SHORT).show();
                return;
            }else if (titulo.getText().toString().isEmpty()) {
                titulo.setError("INGRESE TITULO");
                titulo.requestFocus();
            }else if (ubicacion.getText().toString().isEmpty()) {
                ubicacion.setError("INGRESE UBICACION");
                ubicacion.requestFocus();
            }else if (descripcion.getText().toString().isEmpty()) {
                descripcion.setError("INGRESE DESCRIPCION");
                descripcion.requestFocus();
            }else{
                if(rbGato.isChecked() == true){
                    tipoAnimal = rbGato.getText().toString();
                }else if(rbPerro.isChecked() == true){
                    tipoAnimal = rbPerro.getText().toString();
                }else if(rbOtro.isChecked() == true){
                    tipoAnimal = rbOtro.getText().toString();
                }

                StorageReference file = storage.child("Mascotas").child(uri.getLastPathSegment());
                file.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        final Uri urlFotoAnimal = taskSnapshot.getDownloadUrl();

                        DatosAnimal datosAnimal = new DatosAnimal();
                        datosAnimal.setTitulo(titulo.getText().toString());
                        datosAnimal.setDescripcion(descripcion.getText().toString());
                        datosAnimal.setUbicacion(ubicacion.getText().toString());
                        datosAnimal.setTipoAnimal(tipoAnimal);
                        datosAnimal.setUrlFoto(urlFotoAnimal.toString());
                        datosAnimal.setFecha(compraFecha);
                        datosAnimal.setNombreUsuario(nombreCompleto);

                        databaseReference.child(uuid).setValue(datosAnimal).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ActivityIngresoAnimal.this, "ANIMAL REGISTRADO", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ActivityIngresoAnimal.this, navegacion.class));
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(ActivityIngresoAnimal.this, "NO SE A PODIDO REGISTRADO", Toast.LENGTH_SHORT).show();
                            }
                        });


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uri = result.getUri();
                foto.setImageURI(uri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, result.getError().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void fecha_hora(){
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss a");
                                String dateString = sdf.format(date);
                                compraFecha= dateString;
                            }
                        });
                    }
                } catch (InterruptedException e) {

                }
            }
        };
        t.start();
    }

    private void obtenerUsuario() {

        usuario.child("Usuario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot: dataSnapshot.getChildren()) {
                    String key = usuario.child("Usuario").child(userId).getKey();
                    String key2 = snapshot.getKey();
                    if (key.equals(key2)){
                        Usuario usuario_perfil = snapshot.getValue(Usuario.class);
                        nombreCompleto = usuario_perfil.getNombre() + " " + usuario_perfil.getApellido();
                    }else {

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
}
