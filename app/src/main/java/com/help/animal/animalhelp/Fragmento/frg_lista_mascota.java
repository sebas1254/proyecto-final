package com.help.animal.animalhelp.Fragmento;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.help.animal.animalhelp.Entidades.DatosAnimal;
import com.help.animal.animalhelp.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class frg_lista_mascota extends Fragment {

    private RecyclerView recyclerView;
    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<DatosAnimal, ViewHolder> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frg_lista_mascota, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.ListViewInicio);
        recyclerView.hasFixedSize();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        databaseReference = FirebaseDatabase.getInstance().getReference().child("MASCOTAS");
        databaseReference.keepSynced(true);

        DatabaseReference productosRef = FirebaseDatabase.getInstance().getReference().child("MASCOTAS");
        Query productQuery = productosRef.orderByKey();

        FirebaseRecyclerOptions animalOptions = new FirebaseRecyclerOptions.Builder<DatosAnimal>().setQuery(productQuery, DatosAnimal.class).build();

        adapter = new FirebaseRecyclerAdapter<DatosAnimal, ViewHolder>(animalOptions) {
            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final DatosAnimal model) {
                holder.setTipo("TIPO: " + model.getTipoAnimal());
                holder.setUbicacion("UBICACION: "+model.getUbicacion());
                holder.setDescripcion("DESCRIPCION: "+model.getDescripcion());
                holder.setFoto(getContext(), model.getUrlFoto());

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Fragment fragment = new frg_detalle_mascota();
                        Bundle bundle = new Bundle();
                        bundle.putString("titulo", model.getTitulo());
                        bundle.putString("ubicacion", model.getUbicacion());
                        bundle.putString("descripcion", model.getDescripcion());
                        bundle.putString("tipo", model.getTipoAnimal());
                        bundle.putString("foto", model.getUrlFoto());
                        bundle.putString("publicidad", model.getNombreUsuario());
                        bundle.putString("fecha", model.getFecha());
                        fragment.setArguments(bundle);
                        getFragmentManager().beginTransaction().add(R.id.contenedorFragmento, fragment).addToBackStack(null).commit();
                    }
                });
            }

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_lista_mascota, viewGroup, false);
                return new frg_lista_mascota.ViewHolder(view);
            }
        };

        recyclerView.setAdapter(adapter);
        return view;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        View mView;
        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        public void setTipo(String ubicacion) {
            TextView ubica = (TextView)mView.findViewById(R.id.lblTipo);
            ubica.setText(ubicacion);
        }

        public void setFoto(Context context, String foto) {
            CircleImageView circleImageView = (CircleImageView)mView.findViewById(R.id.animalFoto);
            Picasso.with(context).load(foto).into(circleImageView);
        }

        public void setUbicacion(String ubicacion) {
            TextView ubica = (TextView)mView.findViewById(R.id.lblUbicacion);
            ubica.setText(ubicacion);
        }

        public void setDescripcion(String descripcion) {
            TextView des = (TextView)mView.findViewById(R.id.lblDescricion);
            des.setText(descripcion);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

}
