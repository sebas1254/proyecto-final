package com.help.animal.animalhelp.Entidades;

public class Usuario {
    private String Correo, Nombre, Direcion, Apellido, idUsuario, Foto;

    public Usuario() {
        this.Correo = Correo;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Direcion = Direcion;
        this.idUsuario = idUsuario;
        this.Foto = Foto;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDirecion() {
        return Direcion;
    }

    public void setDirecion(String direcion) {
        Direcion = direcion;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
