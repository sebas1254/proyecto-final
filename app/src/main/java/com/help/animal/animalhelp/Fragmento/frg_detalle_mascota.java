package com.help.animal.animalhelp.Fragmento;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.help.animal.animalhelp.R;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class frg_detalle_mascota extends Fragment {

    private TextView titulo, ubicacion, descripciom, tipo, publicado, fecha;
    private ImageView foto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frg_detalle_mascota, container, false);

        titulo = (TextView) view.findViewById(R.id.detalleTitulo);
        ubicacion = (TextView) view.findViewById(R.id.detalleUbicacion);
        descripciom = (TextView) view.findViewById(R.id.detalleDescripcion);
        tipo = (TextView) view.findViewById(R.id.detalleTipo);
        publicado = (TextView) view.findViewById(R.id.detallePublicado);
        fecha = (TextView) view.findViewById(R.id.detalleFecha);

        foto = (ImageView) view.findViewById(R.id.detalleFoto);

        titulo.setText(getArguments().getString("titulo"));
        ubicacion.setText(getArguments().getString("ubicacion"));
        descripciom.setText(getArguments().getString("descripcion"));
        tipo.setText(getArguments().getString("tipo"));
        publicado.setText(getArguments().getString("publicidad"));
        fecha.setText(getArguments().getString("fecha"));
        Picasso.with(getContext()).load(getArguments().getString("foto")).into(foto);

        return view;
    }

}
