package com.help.animal.animalhelp.Fragmento;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.help.animal.animalhelp.Entidades.DatosAnimal;
import com.help.animal.animalhelp.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class frg_galeria extends Fragment {

    ScaleAnimation shrinkAnim;
    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager mLayoutManager;
    private TextView tvNoMovies;

    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<DatosAnimal, frg_galeria.ViewHolder> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frg_galeria, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        //scale animation to shrink floating actionbar
        shrinkAnim = new ScaleAnimation(1.15f, 0f, 1.15f, 0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        if (recyclerView != null) {
            //to enable optimization of recyclerview
            recyclerView.setHasFixedSize(true);
        }
        //using staggered grid pattern in recyclerview
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.hasFixedSize();
        //recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        databaseReference = FirebaseDatabase.getInstance().getReference().child("MASCOTAS");
        databaseReference.keepSynced(true);

        DatabaseReference productosRef = FirebaseDatabase.getInstance().getReference().child("MASCOTAS");
        Query productQuery = productosRef.orderByKey();

        FirebaseRecyclerOptions animalOptions = new FirebaseRecyclerOptions.Builder<DatosAnimal>().setQuery(productQuery, DatosAnimal.class).build();

        adapter = new FirebaseRecyclerAdapter<DatosAnimal, ViewHolder>(animalOptions) {
            @Override
            protected void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull final DatosAnimal model) {
                holder.setFoto(getContext(), model.getUrlFoto());

                holder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final AlertDialog.Builder dialogo = new AlertDialog.Builder(getContext());
                        LayoutInflater inflater = LayoutInflater.from(getContext());
                        View viewFoto = inflater.inflate(R.layout.layout_foto, null);
                        final ImageView foto = (ImageView) viewFoto.findViewById(R.id.Foto);
                        Picasso.with(getContext()).load(model.getUrlFoto()).into(foto);
                        dialogo.setView(viewFoto);
                        dialogo.show();
                    }
                });
            }

            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_item_galeria_mascota, viewGroup, false);
                return new frg_galeria.ViewHolder(view);
            }
        };
        recyclerView.setAdapter(adapter);

        return view;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvMovieName;
        RatingBar ratingBar;
        ImageView ivMoviePoster;
        View mView;
        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setFoto( Context context, String foto ) {
            ivMoviePoster = (ImageView) mView.findViewById(R.id.iv_movie_poster);
            Picasso.with(context).load(foto).into(ivMoviePoster);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();

    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }


}
