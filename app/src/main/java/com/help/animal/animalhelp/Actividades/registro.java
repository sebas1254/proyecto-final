package com.help.animal.animalhelp.Actividades;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.help.animal.animalhelp.Entidades.Usuario;
import com.help.animal.animalhelp.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class registro extends AppCompatActivity implements View.OnClickListener {

    private EditText Nombre,correo,contraseña,contraseñaRepetida, apellido, direccion, idUsuario;
    private Button Registro, foto;
    private FirebaseAuth mAuth;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private Uri uri;
    private CircleImageView fotoIma;
    private StorageReference storage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        Nombre = (EditText) findViewById(R.id.txtNombres);
        correo = (EditText) findViewById(R.id.txtCorreo);
        apellido = (EditText) findViewById(R.id.txtApellido);
        direccion = (EditText) findViewById(R.id.txtDireccion);
        contraseña = (EditText) findViewById(R.id.txtContraseña);
        contraseñaRepetida = (EditText) findViewById(R.id.txtRcontraseña);

        fotoIma = (CircleImageView) findViewById(R.id.CirclePerfil);

        Registro = (Button) findViewById(R.id.btnRegistrarme);
        foto = (Button) findViewById(R.id.btnFoto);

        uri = null;

        mAuth = FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Usuario");
        storage = FirebaseStorage.getInstance().getReference();

        Registro.setOnClickListener(this);
        foto.setOnClickListener(this);

    }

private void registrarUsuario(){

    if (uri == null) {
        Toast.makeText(this, "DEBE ELEGIR UNA FOTO", Toast.LENGTH_SHORT).show();
        return;
    }else if (Nombre.getText().toString().isEmpty()) {
        Nombre.setError("INGRESE NOMBRE");
        Nombre.requestFocus();
    }else if (apellido.getText().toString().isEmpty()) {
        apellido.setError("INGRESE APELLIDO");
        apellido.requestFocus();
    }else if (direccion.getText().toString().isEmpty()) {
        direccion.setError("INGRESE DIRECCION");
        direccion.requestFocus();
    }else if (correo.getText().toString().isEmpty()) {
        correo.setError("INGRESE CORREO");
        correo.requestFocus();
    }else if (!validarContraseña()) {
        contraseña.setError("CONTRASEÑA NO COINCIDEN");
        contraseña.requestFocus();
    }else{
        StorageReference file = storage.child("FotoPerfil").child(uri.getLastPathSegment());
        file.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                final Uri urlFotoPerfil = taskSnapshot.getDownloadUrl();

                mAuth.createUserWithEmailAndPassword(correo.getText().toString(), contraseña.getText().toString())
                        .addOnCompleteListener(registro.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    Usuario usuario = new Usuario();
                                    usuario.setCorreo(correo.getText().toString());
                                    usuario.setNombre(Nombre.getText().toString());
                                    usuario.setApellido(apellido.getText().toString().trim());
                                    usuario.setDirecion(direccion.getText().toString().trim());
                                    usuario.setIdUsuario(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                    usuario.setFoto(urlFotoPerfil.toString());

                                    databaseReference.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Toast.makeText(registro.this, "se registro correctamente",Toast.LENGTH_SHORT).show();
                                                    finish();
                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(registro.this, "Nose ha podido registrar el usuario",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }else{
                                    Toast.makeText(registro.this, "EL CORREO CON EL QUE SE INTENTA REGISTRAR YA EXISTE", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }
    }

public boolean validarContraseña (){
        String Contraseña,ContraseñaRepetida;
        Contraseña = contraseña.getText().toString();
        ContraseñaRepetida = contraseñaRepetida.getText().toString();
        if (Contraseña.equals(ContraseñaRepetida)){
            if (Contraseña.length() >=6){
                return true;
            }else return false;
        }else return false;
}

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegistrarme:
                registrarUsuario();
                break;
            case R.id.btnFoto:
                CropImage.activity()
                        .setAspectRatio(1, 1)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .start(this);
                break;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                uri = result.getUri();
                fotoIma.setImageURI(uri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, result.getError().toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
